class Solution:
    def isPowerOfFour(self, n: int) -> bool:
        #first  condition (n& (n-1)==0 )  is check the if it is power of 2 
        # second condition is to check when it equal 1 divided by  3 
        #As the power of 4 wbhen dividing with three always give 1 remainder 
        if((n & (n - 1)) == 0) and (n % 3== 1):
            return True
            # print(True)
        else:
            return False
            # print(False)
            
ob=Solution()
print(ob.isPowerOfFour(4))



 

