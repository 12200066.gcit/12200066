class Solution:
	def decode(self,encoded,first:int):
		arr=[first]
		for i in range(0, len(encoded)):
			arr.append(arr[i] ^ encoded[i])
		print(arr)

ob=Solution()
ob.decode([1,2,3],1)
# n=[1,2,3]
# for i in n:
# 	print(i)
# print("")
# for i in range(len(n)):
# 	print(n[i])
